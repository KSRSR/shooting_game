﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class RocketContoroller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bulletPrefab;

    //爆発エフェクト
    public GameObject explosionPrefab;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow)&&gameObject.transform.position.x>=-2.6)
        {
            transform.Translate(-0.1f, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow)&&gameObject.transform.position.x<=2.6)
        {
            transform.Translate(0.1f, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            //弾丸発射音
            GameObject.Find("ALL_C").GetComponent<AudioMake>().AudioM("bl");
        }


    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Finish")
        {
            //爆発エフェクト
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Destroy(coll.gameObject);
            Destroy(gameObject);
            Debug.Log("消滅");
            
            //ゲームオーバー
            GameObject.Find("Canvas").GetComponent<UIcontoroll>().GameOver();
            //音声
            GameObject.Find("ALL_C").GetComponent<AudioMake>().AudioM("Finish");
            //リスタートボタン
            GameObject.Find("Canvas").GetComponent<UIcontoroll>().RestartButton();

        }
        Debug.Log("消滅");
    }
}
    

