﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class UIcontoroll : MonoBehaviour
{

    int score = 0;
    GameObject scoreText;
    GameObject gameOverText;
    public GameObject RestertButton;
    //スコア
    public void AddScore()
    {
        this.score += 10;
    }
    //ゲームオーバー
    public void GameOver()
    {
        this.gameOverText.GetComponent<Text>().text = "GameOver";
    }
    //リスタートボタン　
    public void RestartButton()
    {
        Instantiate (RestertButton , this.transform);
    }
    // Start is called before the first frame update
    void Start()
    {
        this.scoreText = GameObject.Find("Score");
        this.gameOverText = GameObject.Find("GameOver");
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.GetComponent<Text> ().text = "Score:" + score.ToString("D4");
    }
}
