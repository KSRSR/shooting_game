﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioContoroll : MonoBehaviour {


    private AudioSource ausrc;
    public AudioClip bullet;
    public AudioClip dokan;
    public AudioClip innseki;
    public AudioClip Finish;
    public AudioClip BGM1;

    // Start is called before the first frame update
     void Update()
    {
        if (!ausrc.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
    // Update is called once per frame
    public void AudioMaster(string ob)
    {
        ausrc = gameObject.GetComponent<AudioSource>();
        switch (ob)
        {
            case "dokan":
                    ausrc.clip = dokan;
                    ausrc.volume = 1.0f;
                    break;
            case "bl":
                    ausrc.clip = bullet;
                    ausrc.volume = 1.0f;
                    break;
            case "innseki":
                    ausrc.clip = innseki;
                　　ausrc.volume = 1.0f;
                　　break;
            case "Finish":
                    ausrc.clip = Finish;
                    ausrc.volume = 0.7f;
                    break;
            case "BGM1":
                    ausrc.clip = BGM1;
                    ausrc.volume = 0.3f;
                    break;
            default:
                    
                    break;

        }

        //ausrc.PlayOneShot(ausrc.clip);
        ausrc.Play();


    }
}
