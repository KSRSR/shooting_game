﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMake : MonoBehaviour
{
    public GameObject Audio_M;

    // Start is called before the first frame update

    void Start()
    {
        AudioM("BGM1");
    }

    void Update()
    {
        
    }

    // Update is called once per frame
    public void AudioM(string ob )
    {
        
        GameObject obj = Instantiate(Audio_M, this.transform.position, Quaternion.identity);
        obj.GetComponent<AudioContoroll>().AudioMaster(ob);
    }
}
