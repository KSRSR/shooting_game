﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalletControll : MonoBehaviour
{
    public GameObject explosionPrefab;


    // Update is called once per frame



    void Update()
    {
        transform.Translate(0, 0.5f, 0);
        

        if (transform.position.y > 7)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Finish")
        {
            //スコア
            GameObject.Find("Canvas").GetComponent<UIcontoroll>().AddScore();

            //爆発エフェクト
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Destroy(coll.gameObject);
            Destroy(gameObject);

            //爆発音
          
            GameObject.Find("ALL_C").GetComponent<AudioMake>().AudioM("dokan");
        }
    }
}

